% variáveis que vão ser analisadas
MS = [MotorSpeedRPM];
PC = [PhaseCurrentARMS];
MP = [MechanicalPowerkW];
RT = [ReportedTorqueNm];
S_VCU = [SpeedVCUkmh];
D_VCU = [TotalDistanceVCUkm];
% extraindo dados importantes sobre as variáveis e imprimindo-os
disp('MotorSpeed [RPM]= ') 
disp('    max   |   min   |  média  | mediana | desvio padrão ')
disp('  ======================================================')
disp([  max(MS) min(MS) mean(MS) median(MS)  std(MS)])

disp('PhaseCurrent [A RMS]= ')
disp('    max   |   min   |  média  | mediana | desvio padrão ')
disp('  ======================================================')
disp([   max(PC) min(PC) mean(PC) median(PC) std(PC)])

disp('MechanicalPower [kW]= ')
disp('    max   |   min   |  média  | mediana | desvio padrão ')
disp('  ======================================================')
disp([  max(MP) min(MP) mean(MP) median(MP) std(MP)])

disp('ReportedTorque [Nm] = ')
disp('    max   |   min   |  média  |  mediana  | desvio padrão ')
disp('  ======================================================')
disp([  max(RT) min(RT) mean(RT) median(RT) std(RT)])

disp('SpeedVCU [kmh]= ')
disp('    max   |    min    |  média  | mediana | desvio padrão ')
disp('  ======================================================')
disp([  max(S_VCU) min(S_VCU) mean(S_VCU) median(S_VCU) std(S_VCU)])

disp('TotalDistanceVCU [km]= ')
disp('     max   |   min   |  média  |  mediana  | desvio padrão ')
disp('  ==========================================================')
disp([  max(D_VCU) min(D_VCU)  mean(D_VCU) median(D_VCU) std(D_VCU)])

% plotando gráficos sobre variação da variável versus tempo
subplot(3,2,1)
plot(Times, MotorSpeedRPM, "r")
title("MotorSpeed[RPM] vs Tempo");
xlabel("Tempo");
ylabel("MotorSpeed [RPM]");
grid

subplot(3,2,2)
plot( Times, ReportedTorqueNm, "c")
title('Reported Torque[Nm] vs Tempo')
xlabel("Tempo");
ylabel("Reported Torque [Nm]");
grid

subplot(3,2,3)
plot(Times, PhaseCurrentARMS ,"g")
title('Phase Current[A RMS] vs Tempo')
xlabel("Tempo");
ylabel("Phase Current [A RMS]");
grid

subplot(3,2,4)
plot(Times, MechanicalPowerkW,"b")
title("Mechanical Power[kW] vs Tempo")
xlabel("Tempo");
ylabel("Mechanical Power [kW] ");
grid

subplot(3,2,5)
plot( Times, SpeedVCUkmh, "m")
title("Speed VCU[kmh] vs Tempo")
xlabel("Tempo");
ylabel("Speed VCU [kmh]");
grid

subplot(3,2,6)
plot( Times, TotalDistanceVCUkm, "k")
title("Total Distance VCU[km] vs Tempo")
xlabel("Tempo");
ylabel("Total Distance VCU [km]");
grid


